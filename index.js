module.exports = {
  "extends": [
    "plugin:flowtype/recommended"
  ],
  "rules": {
    "flowtype/delimiter-dangle": 2,
    "flowtype/no-dupe-keys": 2,
    "flowtype/no-primitive-constructor-types": 2,
    "flowtype/object-type-delimiter": [2, "comma"],
    "flowtype/require-valid-file-annotation": [2, "always", { "annotationStyle": "line" }],
    "flowtype/semi": [2, "never"],
    "flowtype/type-id-match": [2, "^([A-Z][a-z0-9]*)+Type$"],
    "flowtype/space-before-type-colon": [2, "never"],
    "flowtype/space-after-type-colon": [2, "always"],
    
    "no-duplicate-imports": 0, // replaced with import/no-duplicate
    "import/no-unresolved": 0, // use flow for this inspection
    "import/no-duplicates": 2 // better than no-duplicate-imports
  },
  "plugins": [
    "flowtype"
  ]
}